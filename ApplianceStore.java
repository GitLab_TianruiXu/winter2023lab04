import java.util.Scanner;
public class ApplianceStore {

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		Refrigerator[] appliances = new Refrigerator[4];
		for(int i = 0; i < appliances.length; i ++) {
			System.out.println("Enter the brand, default temperature and days before next clean of the refrigerator by pressing enter after each field");
			appliances[i] = new Refrigerator(userInput.nextLine(), userInput.nextDouble(), userInput.nextInt());
			userInput.nextLine();
		}
//		System.out.println("Last Appliance \n" + "Brand name: " + appliances[3].brand + " \n"+ "Default temperature: " 
//				+ appliances[3].temperature + " \n" + "Default days before next clean: " + appliances[3].daysBeforeNextClean);
//		System.out.println(appliances[0].getBrand());
//		appliances[0].modifyTemperature(-5.0);
//		appliances[0].resetCleanDays();
//		System.out.println("First Appliance \n" + "Brand name: " + appliances[0].brand + " \n"+ "Default temperature: " 
//				+ appliances[0].temperature + " \n" + "Default days before next clean: " + appliances[0].daysBeforeNextClean);
		System.out.println("Enter the updated brand of the last refrigerator");
		appliances[3].setBrand(userInput.nextLine());
		System.out.println("Enter the updated temperature of the last refrigerator");
		appliances[3].setTemperature(userInput.nextDouble());
		System.out.println("Enter the updated days before next clean of the last refrigerator");
		appliances[3].setDaysBeforeNextClean(userInput.nextInt());
		System.out.println("Enter the amount of food added");
		appliances[1].addFood(userInput.nextDouble());
		System.out.println("Amount used: " + appliances[1].getAmountUsed() + "\n" + "Percent used: " + appliances[1].getPercentUsed() + "%");
		System.out.println("Last refrigerator: \n" + "Brand: " + appliances[3].getBrand() 
				+ "\n" + "Temperature: " + appliances[3].getTemperature()
				+ "\n" + "Days before next clean: " + appliances[3].getDaysBeforeNextClean());
	}

}
