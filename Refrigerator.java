
public class Refrigerator {
	private String brand = "";
	private double temperature;
	private int daysBeforeNextClean;
	private int totalCapacity = 100;;
	private double amountUsed;
	private double percentUsed;
	
	public Refrigerator(String brand, double temperature, int daysBeforeNextClean) {
		this.brand = brand;
		this.temperature = temperature;
		this.daysBeforeNextClean = daysBeforeNextClean;
	}
	
	public double getTemperature() {
		return this.temperature;
	}
	
	public int getDaysBeforeNextClean() {
		return this.daysBeforeNextClean;
	}
	
	public int getTotalCapacity() {
		return this.totalCapacity;
	}
	
	public double getAmountUsed() {
		return this.amountUsed;
	}
	
	public double getPercentUsed() {
		return this.percentUsed;
	}
	
	public String getBrand() {
		return this.brand;
	}
	
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
	
	public void setDaysBeforeNextClean(int days) {
		this.daysBeforeNextClean = days;
	}
	
	public void modifyTemperature(double degree) {
		this.temperature -= degree;
	}
	
	public void resetCleanDays() {
		this.daysBeforeNextClean = 90;
	}
	
	public void addFood(double amount) {
		if(isValid(amount)) {
			this.amountUsed += amount;
			this.percentUsed = this.amountUsed / this.totalCapacity * 100;
		}
	}
	
	private boolean isValid(double input) {
		return input > 0 ? true: false;
	}
}
